-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 09:04 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `IOT`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `expires` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `status`, `token`, `expires`) VALUES
(1, 'admin', 'admin', 0, 'af94cec5b0d4408d4f62b3f367b96459', '2019-01-31 01:42:49'),
(2, 'mayuresh', 'mayuresh', 0, '207566e0e3de42d4690bd519a2af057b', '2017-09-28 00:54:07'),
(3, 'mayuersh', 'mayuresh', 0, NULL, NULL),
(4, 'rahul', 'rahul', 0, '0432c2bd44a77ebd235a5c59cbb81ec5', '2017-09-28 01:05:17');

-- --------------------------------------------------------

--
-- Table structure for table `vehicals`
--

CREATE TABLE `vehicals` (
  `id` int(11) NOT NULL,
  `VahicalName` varchar(100) NOT NULL,
  `Vahicalno` int(10) NOT NULL,
  `DriverName` varchar(100) NOT NULL,
  `LicanceNo` int(20) NOT NULL,
  `StartTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EndTime` datetime NOT NULL,
  `Fule` varchar(100) NOT NULL,
  `Kilometers` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicals`
--

INSERT INTO `vehicals` (`id`, `VahicalName`, `Vahicalno`, `DriverName`, `LicanceNo`, `StartTime`, `EndTime`, `Fule`, `Kilometers`) VALUES
(1, 'Suzuki', 32123, 'Loran', 5647890, '2019-01-31 01:21:19', '2019-01-30 06:00:00', '10Litrs', 200),
(2, 'Honda', 32123, 'Max', 5647890, '2019-01-31 01:21:19', '2019-01-30 06:00:00', '10Litrs', 200),
(3, 'Maruti', 32123, 'Zest', 5647890, '2019-01-31 01:21:19', '2019-01-30 06:00:00', '10Litrs', 200);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicals`
--
ALTER TABLE `vehicals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vehicals`
--
ALTER TABLE `vehicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
