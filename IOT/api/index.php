<?php
include 'dbConfig.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$slim_app = new \Slim\Slim();
$slim_app->post('/doLogin','doLogin');
$slim_app->get('/logout','logout');
$slim_app->get('/loadOnlineVehical','loadOnlineVehical');
$slim_app->delete('/deleteRecord/:did','deleteRecord');


$slim_app->run();


function logout() {

}
function doLogin() {
	$request = \Slim\Slim::getInstance()->request();
	$update = json_decode($request->getBody());

	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM admin WHERE username=:username1 AND password=:password1");
		$stmt->bindValue(':username1', $update->username, PDO::PARAM_INT);
		$stmt->bindValue(':password1', $update->password, PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    /****************Gerating token and updating the db***************/
    $cur_time=date("Y-m-d H:i:s");
    //$duration='+10 minutes';
    //$expires = date('Y-m-d H:i:s', strtotime($duration, strtotime($cur_time)));
    $token = md5(microtime() . uniqid(). rand());

    $sql = "UPDATE admin SET token = :token, expires = ADDTIME(now(), '1000') WHERE username=:username1 AND password=:password1";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("username1", $update->username);
    $stmt->bindParam("password1", $update->password);
    $stmt->bindParam("token", $token);
    $status = $stmt->execute();

    /******************************/
		$db = null;

		if(count($rows)) {
			echo '{"status": "success","token": "'.$token.'"}';
		}
		else
			echo '{"status": "failed"}';
	} catch(PDOException $e) {
		echo '{"error":{"msg":'. $e->getMessage() .'}}';
	}

}

function getToken_Validate() {
  $app = \Slim\Slim::getInstance();
  $token = $app->request->headers->get('token');

  //$sql = "SELECT count(id) as ct FROM admin WHERE token=:token AND expires>NOW()";
  $sql = "SELECT count(id) as ct FROM admin WHERE token=:token";

  $db = getDB();
  $stmt = $db->prepare($sql);
  $stmt->bindValue(':token', $token);
  $stmt->execute();
  return $rows = $stmt->fetchColumn(0);
  $db = null;
}
//quarterly
function loadOnlineVehical() {
    $token_exists = getToken_Validate();
    if($token_exists) {

    $sql = "Select id, 
                    VahicalName,
                    Vahicalno,
                    DriverName,
                    LicanceNo,
                    StartTime,
                    EndTime,
                    Fule,
                    Kilometers FROM vehicals ORDER BY id asc";
    try {
    		$db = getDB();
    		$stmt = $db->query($sql);
    		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
    		$db = null;
                echo json_encode($users);
    	} catch(PDOException $e) {
    	    echo '{"error":{"text":'. $e->getMessage() .'}}';
    	}
    } else {
      echo '{"err": "failed"}';
    }
}
function deleteRecord($did) {

	$token_exists = getToken_Validate();
	if($token_exists) {

		$sql = "DELETE FROM vehicals WHERE id=:delete_id";
		try {
			$db = getDB();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("delete_id", $did);
			$stmt->execute();
			$db = null;
			//echo true;
                        loadOnlineVehical();
		} catch(PDOException $e) {
			echo '{"error":{"text":'. $e->getMessage() .'}}';
		}
    } else {
      echo '{"err": "failed"}';
    }
}
?>
