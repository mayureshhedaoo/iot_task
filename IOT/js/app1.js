var base_url ='http://localhost/iot_task/IOT/';

var crudApp = angular.module('crudApp', ['ngRoute','ngSanitize','LocalStorageModule','cfp.loadingBar', 'ngAnimate']);

crudApp.config(function ($routeProvider,localStorageServiceProvider,cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeSpinner = true;
    
    // set up routing
	$routeProvider
		.when('/', { templateUrl : 'templates/login.html' , controller : 'loginpageController' , title : 'Login'})
		.otherwise({ redirectTo: '/'});

    localStorageServiceProvider
    .setPrefix('crudApp')
    .setStorageType('sessionStorage')
    .setStorageCookie(1, base_url)
    .setStorageCookieDomain('')
    .setNotify(true, true);


});





crudApp.controller("loginpageController",function ($scope,$http,localStorageService,cfpLoadingBar) {

    if(localStorageService.isSupported) {
        var storageType = localStorageService.getStorageType(); //e.g localStorage
        //alert(storageType)
    }
	$scope.username = 'admin';
	$scope.password= 'admin';

	$scope.submitloginForm = function () {
		cfpLoadingBar.start();
        var url = base_url + 'api/doLogin';

		var responseData = $http.post(url,{ 'username': $scope.username,'password':$scope.password });
		responseData.success(function(data, status, headers, config) {
			localStorageService.set('token', data.token);

            cfpLoadingBar.complete();
			if(data.status == 'success') {
				location.href = 'mindex.html';
			} else {
				alert('Supplied Credentials are invalid.');
			}

		});
		responseData.error(function(data, status, headers, config) {
			//alert("AJAX failed!");
		});
	};

});
